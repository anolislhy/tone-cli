#!/bin/bash
# Avaliable environment variables:
# $C_VERSION: image version seperated by '#'
# $CONTAINER_ENGINE: container engine for func test seperated by '/'

group_array=$(cat $TONE_CONF_DIR/functional/image-ci-test.conf | grep -v ^group$)

run(){
    [ -n "$group" ] || {
        echo "No group in specfile"
        return 1
    }
    [[ "$group_array" =~  "$group" ]] || {
        echo "$group is not supported"
        return 1
    }

    export PYTHONPATH=$TONE_BM_RUN_DIR/tests
    option=""
    IFS=', ' read -ra run_tags <<< "$TONE_TESTCASES"
    for tag in "${run_tags[@]}"; do
        if [ -n "$tag" ] && [ "$tag" != "null" ] && [ "$tag" != "container_default" ]; then
            option+="-t $tag "
        fi
    done
    generate_yaml
    if [ "$group" == "keentune_application_container_func_test" ]; then
        if echo "$CONTAINER_SERVICE" |grep -q "optimized"; then
            service_name=${CONTAINER_SERVICE/optimized/keentune}
        else
            service_name=$CONTAINER_SERVICE
        fi
        avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/tests/keentune/tc_container_${service_name}_001.py --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml
        cp -rH $HOME/avocado/job-results/latest/test-results/ $TONE_CURRENT_RESULT_DIR/$casename
    else
        for vhd_case in $(find $TONE_BM_RUN_DIR/tests -type f -name "*.py" -exec grep -l "tags=.*container_default" {} +| xargs grep -LE "tags=.*_container_default" ); do
            echo $vhd_case >> /tmp/case_file.txt
        done
        if [ "$group" != "container_default_group" ]; then
            run_tags+=("${group}_default")
        fi
        for element in "${run_tags[@]}"; do
            for vhd_case in $(find $TONE_BM_RUN_DIR/tests -type f -name "*.py" -exec grep -l "tags=.*$element" {} +); do
                echo $vhd_case >> /tmp/case_file.txt
            done
        done
        for vhd_case in $(sort /tmp/case_file.txt | uniq); do
            avocado -V run --nrunner-max-parallel-tasks 1 $vhd_case -m $TONE_BM_RUN_DIR/hosts.yaml 
            casename=$(echo $vhd_case | xargs basename | cut -d. -f1)
            cp -rH $HOME/avocado/job-results/latest/test-results/ $TONE_CURRENT_RESULT_DIR/$casename
        done
        rm -rf /tmp/case_file.txt
    fi

}

parse()
{
    $TONE_BM_SUITE_DIR/parse.py
}

generate_yaml()
{
    registry_addr=(`echo $REGISTRY_ADDR | tr '#' ' '`)
    c_engine=(`echo $CONTAINER_ENGINE | tr '/' ' '`)
cat > hosts.yaml << EOF
hosts: !mux
    localhost:
        host: localhost
registry: !mux
EOF
    for ((i = 0; i < ${#registry_addr[@]}; i++)); do
cat >> hosts.yaml << EOF
    registry$i:
        registry: ${registry_addr[$i]}
EOF
    done

cat >> hosts.yaml << EOF
engines: !mux
EOF
    for ((i = 0; i < ${#c_engine[@]}; i++)); do
cat >> hosts.yaml << EOF
    engine$i:
        engine: ${c_engine[$i]}
EOF
    done
}
