# keentune-cluster
## Description
Keentune is a full stack intelligent optimization product of operating system driven by two wheels of AI algorithm and expert knowledge base. It provides lightweight and cross platform one click performance optimization for mainstream operating systems, optimizes best performance of applications in an intelligent customized operating environment.
keentune runs on one machine, keentune-cluster runs on three machines. 

## Homepage
# gitee
https://gitee.com/anolis/keentuned.git
https://gitee.com/anolis/keentune_brain.git
https://gitee.com/anolis/keentune_target.git
https://gitee.com/anolis/keentune_bench.git

## Version
NA

## Category
functional

## Parameters
- test_branch:master & mastr-open, git branch of keentune code(acops-new)

## Results
testcase: Pass
testcase: Fail

