#!/bin/env python3
import sys
import re

with sys.stdin as stdin:
    for line in stdin:
        match = re.search(r'(tests/\S+)\.py\S+:\s+(\w+)', line)
        if match:
            group = match.groups()
            path, result = (match.group(1,2))
            if result in ['ERROR', 'WARN', 'INTERRUPT', 'CANCEL']:
                result = 'FAIL'
            if result in ['STARTED']:
                continue
            name = path.split('/')[-1]
            print('{}: {}'.format(name, result))

