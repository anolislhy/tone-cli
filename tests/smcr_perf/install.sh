GIT_URL="git@gitee.com:anolis/smc-test.git"

build()
{
        git submodule update --init
}

install()
{
        echo "Just copy file to run directory"
        rm -rf $TONE_BM_RUN_DIR/smc_test
        mkdir $TONE_BM_RUN_DIR/smc_test
        cp -r * $TONE_BM_RUN_DIR/smc_test/

	[ -n "$SERVER" ] && server=${SERVER%% *} || server=127.0.0.1
	[ -n "$CLIENT" ] && client=${CLIENT%% *} || client=127.0.0.1
	echo "Run install on Server: $server"

	if [ "x$rdma_type" == "x" ]; then
		rdma_type="erdma"
                export SMCTEST_DISABLE_ERDMA_DRIVER_INSTALL=1
	fi

	# who i am
	is_server=$(ip route get $server | grep 'dev lo' | wc -l)
	is_client=$(ip route get $client | grep 'dev lo' | wc -l)

	if [ $is_client -gt 0 ]; then
		logger $TONE_BM_RUN_DIR/smc_test/run.sh -a $server -i $client -r client -e $rdma_type -c performance  -p prepare
	fi

	if [ $is_server -gt 0 ]; then
		logger $TONE_BM_RUN_DIR/smc_test/run.sh -a $client -i $server -r server -e $rdma_type -c performance -p prepare
	fi
}
