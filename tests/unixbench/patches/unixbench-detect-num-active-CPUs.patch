diff -ruN Org/UnixBench/Run Upd/UnixBench/Run
--- Org/UnixBench/Run	2023-01-04 11:15:03.054891840 -0500
+++ Upd/UnixBench/Run	2023-01-04 11:14:54.386891840 -0500
@@ -5,6 +5,7 @@
 use POSIX qw(strftime);
 use Time::HiRes;
 use IO::Handle;
+use FindBin;
 
 
 ############################################################################
@@ -83,21 +84,19 @@
 
 # Establish full paths to directories.  These need to be full pathnames
 # (or do they, any more?).  They can be set in env.
-# variables whose names are the first parameter to getDir() below.
-my $BASEDIR = `pwd`;
-chomp($BASEDIR);
+# variable names are the first parameter to getDir() below.
 
 # Directory where the test programs live.
-my $BINDIR = getDir('UB_BINDIR', $BASEDIR . "/pgms");
+my $BINDIR = getDir('UB_BINDIR', $FindBin::Bin . "/pgms");
 
 # Temp directory, for temp files.
-my $TMPDIR = getDir('UB_TMPDIR', $BASEDIR . "/tmp");
+my $TMPDIR = getDir('UB_TMPDIR', $FindBin::Bin . "/tmp");
 
 # Directory to put results in.
-my $RESULTDIR = getDir('UB_RESULTDIR', $BASEDIR . "/results");
+my $RESULTDIR = getDir('UB_RESULTDIR', $FindBin::Bin . "/results");
 
 # Directory where the tests are executed.
-my $TESTDIR = getDir('UB_TESTDIR', $BASEDIR . "/testdir");
+my $TESTDIR = getDir('UB_TESTDIR', $FindBin::Bin . "/testdir");
 
 
 ############################################################################
@@ -106,10 +105,10 @@
 
 # Configure the categories to which tests can belong.
 my $testCats = {
-    'system'    => { 'name' => "System Benchmarks", 'maxCopies' => 16 },
+    'system'    => { 'name' => "System Benchmarks", 'maxCopies' => 0 },
     '2d'        => { 'name' => "2D Graphics Benchmarks", 'maxCopies' => 1 },
     '3d'        => { 'name' => "3D Graphics Benchmarks", 'maxCopies' => 1 },
-    'misc'      => { 'name' => "Non-Index Benchmarks", 'maxCopies' => 16 },
+    'misc'      => { 'name' => "Non-Index Benchmarks", 'maxCopies' => 0 },
 };
 
 
@@ -671,31 +670,85 @@
 # array of N entries, one per CPU, where each entry is a hash containing
 # these fields:
 # describing the model etc.  Returns undef if the information can't be got.
+#
+# future: on systems without /proc/cpuinfo, might check for Perl modules:
+#   Sys::Info::Device::CPU or Sys::CpuAffinity
 sub getCpuInfo {
-    open(my $fd, "<", "/proc/cpuinfo") || return undef;
+    if (!("$^O" eq "darwin")) {
+        open(my $fd, "<", "/proc/cpuinfo") || return undef;
 
-    my $cpus = [ ];
-    my $cpu = 0;
-    while (<$fd>) {
-        chomp;
-        my ( $field, $val ) = split(/[ \t]*:[ \t]*/);
-        next if (!$field || !$val);
-        if ($field eq "processor") {
-            $cpu = $val;
-        } elsif ($field eq "model name") {
-            my $model = $val;
-            $model =~ s/  +/ /g;
+        my $cpus = [ ];
+        my $cpu = 0;
+        while (<$fd>) {
+            chomp;
+            my ( $field, $val ) = split(/[ \t]*:[ \t]*/);
+            next if (!$field || !$val);
+            if ($field eq "processor") {
+                $cpu = $val;
+            } elsif ($field eq "model name") {
+                my $model = $val;
+                $model =~ s/  +/ /g;
+                $cpus->[$cpu]{'model'} = $model;
+            } elsif ($field eq "bogomips") {
+                $cpus->[$cpu]{'bogo'} = $val;
+            } elsif ($field eq "flags") {
+                $cpus->[$cpu]{'flags'} = processCpuFlags($val);
+            }
+        }
+
+        close($fd);
+
+        $cpus;
+
+    } else {
+
+        my $model = getCmdOutput("sysctl -n machdep.cpu.brand_string");
+        my $flags = getCmdOutput("sysctl -n machdep.cpu.features | tr [A-Z] [a-z]");
+        my $ncpu  = getCmdOutput("sysctl -n hw.ncpu");
+
+        my $cpus = [ ];
+        my $cpu = 0;
+
+        for ($cpu = 0; $cpu < $ncpu; $cpu++) {
             $cpus->[$cpu]{'model'} = $model;
-        } elsif ($field eq "bogomips") {
-            $cpus->[$cpu]{'bogo'} = $val;
-        } elsif ($field eq "flags") {
-            $cpus->[$cpu]{'flags'} = processCpuFlags($val);
+            $cpus->[$cpu]{'bogo'}  = 0;
+            $cpus->[$cpu]{'flags'} = processCpuFlags($flags);
         }
+        $cpus;
     }
+}
 
-    close($fd);
 
-    $cpus;
+# Get number of available (active) CPUs (not including disabled CPUs)
+# or, if not num of available CPUs, the total number of CPUs on the system
+# Returns undef if the information can't be obtained.
+#
+# There is no shortage of platform-specific methods to obtain this info.
+# This routine -is not- exhaustive, but adds some additional portability.
+# Most modern unix systems implement sysconf(_SC_NPROCESSORS_ONLN).
+sub getNumActiveCpus {
+    my $numCpus;
+
+    #(POSIX::_SC_NPROCESSORS_ONLN value not typically provided by POSIX.pm)
+    #$numCpus = POSIX::sysconf(POSIX::_SC_NPROCESSORS_ONLN);
+    #if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }
+
+    $numCpus = `getconf _NPROCESSORS_ONLN 2>/dev/null`;
+    if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }
+
+    $numCpus = `getconf NPROCESSORS_ONLN 2>/dev/null`;
+    if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }
+
+    $numCpus = `nproc 2>/dev/null`;
+    if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }
+
+    $numCpus = `python -c 'import os; print os.sysconf(os.sysconf_names["SC_NPROCESSORS_ONLN"]);' 2>/dev/null`;
+    if (defined($numCpus)) { chomp $numCpus; return $numCpus if $numCpus; }
+
+    # Windows
+    return $ENV{"NUMBER_OF_PROCESSORS"} if $ENV{"NUMBER_OF_PROCESSORS"};
+
+    return undef;
 }
 
 
@@ -723,7 +776,7 @@
     $info->{'osRel'} = getCmdOutput("uname -r");
     $info->{'osVer'} = getCmdOutput("uname -v");
     $info->{'mach'} = getCmdOutput("uname -m");
-    $info->{'platform'} = getCmdOutput("uname -i");
+    $info->{'platform'} = getCmdOutput("uname -i") || "unknown";
 
     # Get the system name (SUSE, Red Hat, etc.) if possible.
     $info->{'system'} = $info->{'os'};
@@ -735,9 +788,9 @@
 
     # Get the language info.
     my $lang = getCmdOutput("printenv LANG");
-    my $map = getCmdOutput("locale -k LC_CTYPE | grep charmap");
+    my $map = getCmdOutput("locale -k LC_CTYPE | grep charmap") || "";
     $map =~ s/.*=//;
-    my $coll = getCmdOutput("locale -k LC_COLLATE | grep collate-codeset");
+    my $coll = getCmdOutput("locale -k LC_COLLATE | grep collate-codeset") || "";
     $coll =~ s/.*=//;
     $info->{'language'} = sprintf "%s (charmap=%s, collate=%s)",
                                    $lang, $map, $coll;
@@ -749,11 +802,17 @@
         $info->{'numCpus'} = scalar(@$cpus);
     }
 
+    # Get available number of CPUs (not disabled CPUs), if possible.
+    my $numCpus = getNumActiveCpus();
+    if (defined($numCpus)) {
+        $info->{'numCpus'} = $numCpus; # overwrite value from getCpuinfo()
+    }
+
     # Get graphics hardware info.
-    #$info->{'graphics'} = getCmdOutput("3dinfo | cut -f1 -d\'(\'");
+    $info->{'graphics'} = getCmdOutput("3dinfo | cut -f1 -d\'(\'");
 
     # Get system run state, load and usage info.
-    $info->{'runlevel'} = getCmdOutput("runlevel | cut -f2 -d\" \"");
+    $info->{'runlevel'} = getCmdOutput("who -r | awk '{print \$3}'");
     $info->{'load'} = getCmdOutput("uptime");
     $info->{'numUsers'} = getCmdOutput("who | wc -l");
 
@@ -930,12 +989,12 @@
 
         # If $timebase is 0 the figure is a rate; else compute
         # counts per $timebase.  $time is always seconds.
-        if ($timebase > 0) {
+        if ($timebase > 0 && $time > 0) {
             $sum += $count / ($time / $timebase);
-            $product += log($count) - log($time / $timebase);
+            $product += log($count) - log($time / $timebase) if ($count > 0);
         } else {
             $sum += $count;
-            $product += log($count);
+            $product += log($count) if ($count > 0);
         }
         $totalTime += $time;
     }
@@ -1013,7 +1072,7 @@
         $bresult->{'index'} = $ratio * 10;
 
         # Sun the scores, and count this test for this category.
-        $sum->{$cat} += log($ratio);
+        $sum->{$cat} += log($ratio) if ($ratio > 0.000001);
         ++$indexed->{$cat};
     }
 
@@ -1329,7 +1388,7 @@
         # If the benchmark doesn't want to run with this many copies, skip it.
         my $cat = $params->{'cat'};
         my $maxCopies = $testCats->{$cat}{'maxCopies'};
-        next if ($copies > $maxCopies);
+        next if ($maxCopies > 0 && $copies > $maxCopies);
 
         # Run the benchmark.
         my $bresult = runBenchmark($bench, $params, $verbose, $logFile, $copies);
@@ -1764,7 +1823,7 @@
         $tests = $index;
     }
 
-    #preChecks();
+    preChecks();
     my $systemInfo = getSystemInfo();
 
     # If the number of copies to run was not set, set it to 1
