#!/bin/bash

TONE_CAN_SKIP_PKG="yes"
DEP_PKG_LIST="python36 python36-devel golang rust nginx openssl-devel git kernel-headers"

nginx_conf=/etc/nginx/nginx.conf

set_python_env()
{
    ulimit -n 655350
    pip3 install --upgrade pip
    pip3 install Cython setuptools-rust selenium --timeout 1800
    pip3 install numpy tornado hyperopt==0.2.5 requests scikit_learn pynginxconfig shap xgboost pySOT --timeout 1800
    pip3 install torch --extra-index-url https://download.pytorch.org/whl/cpu 
    pip3 install gpytorch
}

update_yum_source()
{
    sed -i "s/enabled=.*/enabled=1/g" /etc/yum.repos.d/AnolisOS-Plus.repo
    yum clean all
    yum makecache
    yum install -y httpd
    sed -i "0,/Require all denied*/s//#Require all denied/" /etc/httpd/conf/httpd.conf
    sed -i "0,/Listen 80/s//#Listen 80/" /etc/httpd/conf/httpd.conf
    sed -i ':a;N;$!ba;s%CustomLog \"logs/access_log\" combined*%#CustomLog \"logs/access_log\" combined%'  /etc/httpd/conf/httpd.conf
    sed -i "0,/LogLevel warn*/s//LogLevel crit/" /etc/httpd/conf/httpd.conf
    systemctl restart httpd

    yum install -y go
    go env -w GO111MODULE=on
    go env -w GOPROXY=https://goproxy.cn,direct
    yum install python3-systemd systemd-devel -y
}

install_chrome_env()
{
    rpm -ql google-chrome-stable && return 0
    yum install -y https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
    if [ $? = 0 ];then
        wget http://npm.taobao.org/mirrors/chromedriver/2.41/chromedriver_linux64.zip
    fi

    if [ $? = 0 ];then
        unzip chromedriver_linux64.zip
    fi

    mv chromedriver /usr/bin/
    chmod +x /usr/bin/chromedriver
    rm -rf chromedriver_linux64.zip
}

install_wrk()
{
    which wrk && return 0
    git clone https://gitee.com/mirrors/wrk.git wrk
    cd wrk
    make && cp wrk /usr/bin/
    cd -
}

set_nginx_env()
{
    [ -s ${nginx_conf}_bak ] || cp $nginx_conf ${nginx_conf}_bak
    # listen       [::]:80;
    sed -i "/^\s\+listen\s\+\[::\]/d" $nginx_conf
    # access_log  /var/log/nginx/access.log  main;
    sed -i "s/access_log.*/access_log off;/g" $nginx_conf
    systemctl restart nginx
    systemctl status nginx || return 1
}

get_keentune_code()
{
    cd $TONE_BM_RUN_DIR/
    keentune_addr="https://gitee.com/anolis/keentuned.git"
    keentune_brain_addr="https://gitee.com/anolis/keentune_brain.git"
    keentune_target_addr="https://gitee.com/anolis/keentune_target.git"
    keentune_bench_addr="https://gitee.com/anolis/keentune_bench.git"

    git clone -b $keentune_branch $keentune_addr
    git clone -b $keentune_branch $keentune_brain_addr
    git clone -b $keentune_branch $keentune_target_addr
    git clone -b $keentune_branch $keentune_bench_addr

    mv keentune_brain keentune-brain
    mv keentune_target keentune-target
    mv keentune_bench keentune-bench
}

build()
{
    set_python_env
    update_yum_source
    install_chrome_env
    install_wrk
    set_nginx_env
    get_keentune_code
}

install()
{
    echo "The test environment is ready"
}