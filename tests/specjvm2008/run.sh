#!/bin/bash

mode=${mode:-"base"}

setup()
{
    # prepare
    TEST_JVM_OPTIONS=""
    memory=$(dmidecode -t memory | grep Size: | grep -v 'No Module Installed\|Volatile\|Logical\|Cache'|awk '{sum += $2};END {if ( "MB"==$NF ) print sum/1024;else if ( "GB"==$NF ) print sum}')
    if [[ $memory  == "16" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms12g -Xmx12g -Xmn10g"
    elif [[ $memory == "32" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms25g -Xmx25g -Xmn20g"
    elif [[ $memory == "64" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms50g -Xmx50g -Xmn40g"
    elif [[ $memory == "128" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms100g -Xmx100g -Xmn90g"
    elif [[ $memory == "256" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms200g -Xmx200g -Xmn190g"
    elif [[ $memory == "384" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms300g -Xmx300g -Xmn290g"
    elif [[ $memory == "512" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms400g -Xmx400g -Xmn390g"
    else
        xms=$(($memory * 75 / 100))
        xmn=$(($xms * 75 / 100))
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms'$xms'g -Xmx'$xms'g -Xmn'$xmn'g"
    fi

    [[ "$mode" = "base" ]] && TEST_JVM_OPTIONS=""
    # clean up before test
    teardown

    [[ -n $JAVA_HOME ]] || JAVA_HOME=/home
    if [ $Java == "openjdk8" ];then
        java_version="1.8.0"
    else
        java_version="11"
    fi
    os_version=$(cat /etc/os-release | grep ID= | grep -v platform |awk -F "\\\"" "{print \$2}" |xargs)
    [[ $os_version != "anolis 23" ]] && pkg_name=openjdk || pkg_name=alibaba-dragonwell
    java_pkgs="java-$java_version-$pkg_name-headless java-$java_version-$pkg_name-devel java-$java_version-$pkg_name"
    for java_pkg in $java_pkgs; do
        yum -y install $java_pkg
    done
    java_pkg_name=$(rpm -qa | grep java-$java_version-openjdk-[0-9])
    [[ $os_version == "anolis 23" ]] && java_pkg_name=$(rpm -qa | grep java-$java_version-alibaba-dragonwell-[0-9])
    init_install_path=/usr/lib/jvm/$java_pkg_name
    cp -rf $init_install_path $JAVA_HOME
    JAVA_HOME=$JAVA_HOME/$java_pkg_name
    echo "export JAVA_HOME=$JAVA_HOME" >> /etc/profile
    echo "export PATH=$JAVA_HOME/bin:\$PATH" >> /etc/profile
    source /etc/profile
    echo "$(which javac)"
    echo "$(java -version)"
    cd $TONE_BM_RUN_DIR
    java -jar SPECjvm2008_1_01_setup.jar -i console  < SETUP_CONFIG
}

run()
{
    #start to test
    benchmarks='startup.helloworld startup.compiler.compiler startup.compress startup.crypto.aes
    startup.crypto.rsa startup.crypto.signverify startup.mpegaudio startup.scimark.fft startup.scimark.lu
    startup.scimark.monte_carlo startup.scimark.sor startup.scimark.sparse startup.serial startup.sunflow
    startup.xml.transform startup.xml.validation compiler.compiler compress crypto.aes crypto.rsa
    crypto.signverify derby mpegaudio scimark.fft.large scimark.lu.large scimark.sor.large
    scimark.sparse.large scimark.fft.small scimark.lu.small scimark.sor.small scimark.sparse.small
    scimark.monte_carlo serial xml.transform xml.validation'
    cd /SPECjvm2008
    ./run-specjvm.sh $benchmarks
    # clean java_config
    sed -i "/$java_pkg_name/d" /etc/profile
    source /etc/profile
}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.awk
}

teardown()
{
    ps -ef|grep -w "SPECjvm2008.jar"| grep -v "grep" | awk '{print $2}'|xargs -I {} kill -9 {} > /dev/null 2>&1
}

