BENCH_DIR="sysbench-1.0.20"
WGET_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/sysbench-1.0.20.tar.gz"

if [[ "ubuntu debian uos kylin" =~  $TONE_OS_DISTRO ]];then
    DEP_PKG_LIST="vim make automake libtool pkg-config libaio-dev"
else
    DEP_PKG_LIST="vim make automake libtool pkgconfig libaio-devel"
fi
if [[ `uname -p` == "loongarch64" ]]; then
	DEP_PKG_LIST="$DEP_PKG_LIST luajit"
fi

fetch()
{
    wget_download ${WGET_URL}
}

build() {
    tar -zxf $(basename ${WGET_URL})
    cd ${BENCH_DIR}
    patch -p1 < $TONE_BM_SUITE_DIR/0001-add-loongarch64-support-for-concurrency_kit.patch
    ./autogen.sh
    if [[ `uname -p` == "loongarch64" ]]; then
        ./configure --prefix=$TONE_BM_RUN_DIR --without-mysql --with-system-luajit
    else
        ./configure --prefix=$TONE_BM_RUN_DIR --without-mysql
    fi
    make
}

install() {
    make install
}
