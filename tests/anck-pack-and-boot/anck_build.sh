#!/bin/bash

KERNEL_CI_REPO_URL=$2
KERNEL_CI_REPO_BRANCH=$3
KERNEL_CI_PR_ID=$4
CK_BUILDER_REPO="https://gitee.com/src-anolis-sig/ck-build.git"

show_result()
{
    local case_name=$1
    local ret=$2
    local show_str=$3

    [ -n "$show_str" ] && echo "$show_str"
    if [ $ret -ne 0 ]; then
        echo "$case_name: fail"
        exit 1
    else
        echo "$case_name: pass"
        exit 0
    fi
}


if [ $(arch) == "x86_64" ]; then
    kernel_arch="x86"
elif [ $(arch) == "aarch64" ]; then
    kernel_arch="arm64"
else
    show_result $1 1 "Error: Not supported arch"
fi

[ -n "$KERNEL_CI_REPO_URL" ] || {
    show_result $1 1 "Error: KERNEL_CI_REPO_URL is not set"
}
[ -n "$KERNEL_CI_REPO_BRANCH" ] || {
    show_result $1 1 "Error: KERNEL_CI_REPO_BRANCH is not set"
}
[ -n "$KERNEL_CI_PR_ID" ] || {
    echo "Warning: KERNEL_CI_PR_ID is not set"
}

builder_dep_pkg="glibc-static flex bison elfutils-libelf-devel openssl-devel dwarves"
[ -n "$CK_BUILDER_BRANCH" ] || {
    if echo "$KERNEL_CI_REPO_BRANCH" | grep -q "4.19"; then
        CK_BUILDER_BRANCH="an8-4.19"
    elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "5.10"; then
        CK_BUILDER_BRANCH="an8-5.10"
    elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "6.1"; then
        CK_BUILDER_BRANCH="an23-6.1"
        builder_dep_pkg="$builder_dep_pkg bc elfutils-devel libnl3-devel perl-devel rsync"
    elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "6.6"; then
        CK_BUILDER_BRANCH="an23-6.6"
        cat > /etc/yum.repos.d/build.repo <<EOF
[build]
name=build
baseurl=http://8.131.87.1/kojifiles/repos/dist-an23-build/latest/$(uname -m)/
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ANOLIS
gpgcheck=0
EOF
        builder_dep_pkg="$builder_dep_pkg bc elfutils-devel libnl3-devel perl-devel rsync perl-generators"
    else
        show_result $1 1 "Error: KERNEL_CI_REPO_BRANCH:$KERNEL_CI_REPO_BRANCH is not support"
    fi
}


job_num=$(cat /proc/cpuinfo | grep processor | wc | awk {'print $1-1'})

mkdir -p /anck_build
cd /anck_build
anck_repo=$(basename $KERNEL_CI_REPO_URL .git)
[ -d "$anck_repo" ] && {
    echo "$anck_repo exist, try to remove it..."
    rm -rf $anck_repo
}

echo "===> Clone kernel repository..."
git clone --depth 1 -b $KERNEL_CI_REPO_BRANCH $KERNEL_CI_REPO_URL
if [ "X$KERNEL_CI_PR_ID" == "X" ]; then
    echo "===> KERNEL_CI_PR_ID not set, Skip apply patch"
    cd $anck_repo
else
    patch_url="${KERNEL_CI_REPO_URL/\.git/}/pulls/${KERNEL_CI_PR_ID}.patch"
    echo "===> Get the patch from: $patch_url"
    [ -f $(basename $patch_url) ] && rm -f $(basename $patch_url)
    echo "CMD: wget $patch_url"
    wget $patch_url
    cat $(basename $patch_url)
    cd $anck_repo
    echo "===> Apply patch: $patch_url"
    echo "CMD: git config user.email \"test@test.com\""
    echo "CMD: git config user.name \"test\""
    git config user.email "test@test.com"
    git config user.name "test"
    echo "CMD: git am ../$(basename $patch_url)"
    if git am ../$(basename $patch_url); then
    	echo "Apply patch pass: $patch_url"
    else
        show_result $1 1 "Apply patch fail: $patch_url"
    fi
    echo "===> Apply patch done"
fi

echo "===> Install related packages..."
echo "CMD: yum install -y $builder_dep_pkg"
yum install -y $builder_dep_pkg
echo "===> Install packages done"

if [ $1 == "build_allyes_config" ]; then
    echo "== Build Kernel with allyesconfig =="
    echo "make clean && make allyesconfig && make -j $job_num -s && make modules -j $job_num -s"
    make clean && make allyesconfig && make -j $job_num -s && make modules -j $job_num -s
    show_result $1 $?
elif [ $1 == "build_anolis_defconfig" ]; then
    echo "== Build Kernel with anolis_defconfig =="
    echo "CMD: make clean && make anolis_defconfig && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s"
    make clean && make anolis_defconfig && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s
    show_result $1 $?
elif [ $1 == "build_allno_config" ]; then
    echo "== Build Kernel with allnoconfig =="
    echo "CMD: make clean && make allnoconfig && make -j $job_num -s"
    make clean && make allnoconfig && make -j $job_num -s
    show_result $1 $?
elif [ $1 == "build_anolis_debug_defconfig" ]; then
    echo "== Build Kernel with anolis-debug_defconfig =="
    echo "CMD: make clean && make anolis-debug_defconfig && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s"
    make clean && make anolis-debug_defconfig && make olddefconfig && make -j $job_num -s &&  make modules -j $job_num -s
    show_result $1 $?
elif [ $1 == "check_Kconfig" ]; then
    echo "== Check kconfig =="
    check_status=0
    for cfg in $(ls arch/${kernel_arch}/configs/anolis_*);
    do
        echo "Check $cfg"
        echo "CMD: cp -f $cfg .config && make ARCH=${kernel_arch} listnewconfig | grep -E '^CONFIG_' > .newoptions"
        cp -f $cfg .config && make ARCH=${kernel_arch} listnewconfig | grep -E '^CONFIG_' > .newoptions
        [ -s .newoptions ] && cat .newoptions && check_status=1;
    done
    show_result $1 $check_status
elif [ $1 == "anck_rpm_build" ]; then
    echo "== Build Kernel RPM =="
    echo "Get ck builder..."
    cd /anck_build
    ck_builder=$(basename $CK_BUILDER_REPO .git)
    [ -d "$ck_builder" ] && rm -rf $ck_builder
    git clone -b $CK_BUILDER_BRANCH $CK_BUILDER_REPO || return 1
    [ _"$ck_builder" != "_ck-build" ] && ln -s $ck_builder ck-build
    cd $ck_builder
    # for an8-4.19
    if grep ^srcdir build.sh | grep -q ck.git; then
        ln -sf ../${anck_repo} ck.git
    fi
    # for an8-5.10, an23-6.1
    if grep ^srcdir build.sh | grep -q cloud-kernel; then
        ln -sf ../${anck_repo} cloud-kernel
    fi

    [ -d "outputs" ] && rm -rf outputs
    yum install -y yum-utils
    yum-builddep -y kernel.spec
    echo "Build ANCK..."
    [ -n "$BUILD_EXTRA" ] || export BUILD_EXTRA="debuginfo"
    sh build.sh
    show_result $1 $?
fi
