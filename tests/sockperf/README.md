# sockperf

## Description

**sockperf** is a network benchmarking utility over socket API that was designed for testing performance (latency and throughput) of high-performance systems (it is also good for testing performance of regular networking systems). It covers most of the socket API calls and options.

## Homepage

https://github.com/Mellanox/sockperf

## Version

sockperf_v2

## Category

Performance

## Parameters

* protocol: network protocol, such as tcp/udp
* test: sockperf test
  * pp: latency test in ping pong mode
  * ul: latency under load test
  * tp: throughput test
* msg_size: message size for network, mostly used for throughput test
* runtime: run time

## Result

* pp and ul testing, the result are: 
  * avg-lat: avg-latency, smaller is better
  * std-dev: for reference only
* tp testing:
  * msg_per_sec: Message Rate
  * BandWidth_MBps: BandWidth