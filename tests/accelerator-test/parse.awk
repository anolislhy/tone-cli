#!/usr/bin/awk -f
 
/ Pass$/ {
  printf("%s Pass\n", $1)
}
/ Fail$/ {
  printf("%s Fail\n", $1)
}
/ Skip$/ {
  printf("%s Skip\n", $1)
}

