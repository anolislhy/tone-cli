# stress-ng

## Description

stress-ng will stress test a computer system in various selectable ways. It
was designed to exercise various physical subsystems of a computer as well as
the various operating system kernel interfaces.

## Homepage
[https://gitee.com/anolis/stress-ng.git](https://gitee.com/anolis/stress-ng.git)

Mirrored from upstream stress-ng project:
[https://github.com/ColinIanKing/stress-ng](https://github.com/ColinIanKing/stress-ng)


## Version
master

## Category
functional

## Parameters

## Results

## Manual Run
```
1. Run class stressor, class:cpu memory vm io filesystem scheduler pipe os network
stress-ng --class $class --sequential $nr_cpu --timeout 60 --metrics --times --verify

2. Run class stressor, excludes some stressor
stress-ng --class $class --sequential $nr_cpu --timeout 60 --metrics --times --verify -x xxx

3. Run class stressor, specify test directory
stress-ng --class $class --sequential $nr_cpu --timeout 60 --metrics --times --verify --temp-path xxx

```
