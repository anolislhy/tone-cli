# pkg-smoke
### Description
Smoke tests of Anolis OS baseos packages:
  * yum update
  * yum repoclosure
  * rpm install/uninstall

### Homepage
N/A

### Version
0.1

### Parameters
N/A

### Results
- yum_check: pass/fail
- yum_update: pass/fail
- yum_repoclosure: pass/fail
- <repo/pkg>: pass/fail

### Manual Run
N/A
