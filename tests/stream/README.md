# stream_bclinux
## Description
The STREAM benchmark is a simple synthetic benchmark program that measures sustainable memory bandwidth (in MB/s) and the corresponding computation rate for simple vector kernels.

## Homepage
https://github.com/jeffhammond/STREAM.git

## Version
5.10
last commit 1a78bcc

## Category
performance

## Parameters
Na

## Results
```
Copy: 52527.30 MB/s
Scale: 51150.00 MB/s
Add: 54932.20 MB/s
Triad: 54932.20 MB/s

```
