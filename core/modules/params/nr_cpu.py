#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from utils.log import logger
from module_interfaces import PARAM
from multiprocessing import cpu_count


class nr_cpu(PARAM):
    def get(self, value):
        if value[-1] == '%':
            self.value = int(float(value[0:-1]) / 100 * cpu_count())
        else:
            self.value = int(value)
        return self.value
