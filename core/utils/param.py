#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dispatcher import ParamDispatcher


class Default(object):
    _instance = None

    def __new__(cls, *args, **kw):
        if cls._instance is None:
            cls._instance = object.__new__(cls, *args, **kw)
        return cls._instance

    def get(self, value):
        return value


class ParamConvert(ParamDispatcher):
    _instance = None

    def __new__(cls, *args, **kw):
        if cls._instance is None:
            cls._instance = object.__new__(cls, *args, **kw)
        return cls._instance

    def __init__(self):
        super(ParamConvert, self).__init__()

    def __iter__(self):
        return iter([m.name for m in self.modules])

    def __getitem__(self, name):
        for ext in self.modules:
            if ext.name == name:
                return ext.obj
        return Default()
