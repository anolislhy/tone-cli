#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import re

base_pkg_mapping = {
    "ubuntu": "tar wget zip unzip git gcc gawk numactl patch",
    "debian": "tar wget zip unzip git gcc gawk numactl patch",
    "kylin": "tar wget zip unzip git gcc gawk numactl patch",
    "uos": "tar wget zip unzip git gcc gawk patch",
    "anolis": "tar wget zip unzip git gcc numactl patch",
    "alinux": "tar wget zip unzip git gcc numactl patch",
    "bclinux": "tar wget zip unzip git gcc numactl patch",
    "centos": "tar wget zip unzip git gcc numactl patch",
    "Deepin": "tar wget zip unzip git gcc gawk numactl patch",
    "culinux": "tar wget zip unzip git gcc numactl patch",
    "rhel": "tar wget zip unzip git gcc numactl patch"
}

major_version = sys.version_info[0]

pip_mapping = {
    "ubuntu": "python{}-pip".format("" if major_version==2 else 3),
    "debian": "python{}-pip".format("" if major_version==2 else 3),
    "uos": "python{}-pip".format("" if major_version==2 else 3),
    "kylin": "python{}-pip".format(2 if major_version==2 else 3),
    "alinux": "python{}-pip".format(2 if major_version==2 else 3),
    "anolis": "python{}-pip".format(2 if major_version==2 else 3),
    "bclinux": "python{}-pip".format(2 if major_version==2 else 3),
    "centos": "python{}-pip".format(2 if major_version==2 else 3),
    "Deepin": "python{}-pip".format("" if major_version==2 else 3),
    "culinux": "python{}-pip".format(2 if major_version==2 else 3),
    "rhel": "python{}-pip".format(2 if major_version==2 else 3)
}

pydev_mapping = {
    "ubuntu": "python{}-dev".format("" if major_version==2 else 3),
    "debian": "python{}-dev".format("" if major_version==2 else 3),
    "uos": "python{}-dev".format("" if major_version==2 else 3),
    "kylin": "python{}-dev".format(2 if major_version==2 else 3),
    "alinux": "python{}-devel".format(2 if major_version==2 else 3),
    "anolis": "python{}-devel".format(2 if major_version==2 else 3),
    "bclinux":"python{}-devel".format(2 if major_version==2 else 3),
    "centos": "python{}-devel".format(2 if major_version==2 else 3),
    "Deepin": "python{}-dev".format("" if major_version==2 else 3),
    "culinux": "python{}-devel".format(2 if major_version==2 else 3),
    "rhel": "python{}-devel".format(2 if major_version==2 else 3)
}

psutil_mapping = {
    "ubuntu": "python{}-psutil".format("" if major_version==2 else 3),
    "debian": "python{}-psutil".format("" if major_version==2 else 3),
    "uos": "python{}-psutil".format("" if major_version==2 else 3),
    "kylin": "python{}-psutil".format(2 if major_version==2 else 3),
    "alinux": "python{}-psutil".format(2 if major_version==2 else 3),
    "anolis": "python{}-psutil".format(2 if major_version==2 else 3),
    "bclinux": "python{}-psutil".format(2 if major_version==2 else 3),
    "centos": "python{}-psutil".format(2 if major_version==2 else 3),
    "Deepin": "python{}-psutil".format("" if major_version==2 else 3),
    "culinux": "python{}-psutil".format(2 if major_version==2 else 3),
    "rhel": "python{}-psutil".format(2 if major_version==2 else 3)
}

pyyaml_mapping = {
    "ubuntu": "python{}-yaml".format("" if major_version==2 else 3),
    "debian": "python{}-yaml".format("" if major_version==2 else 3),
    "uos": "python{}-pretty-yaml".format("" if major_version==2 else 3),
    "kylin": "python{}-yaml".format(2 if major_version==2 else 3),
    "alinux": "python{}-pyyaml".format(2 if major_version==2 else 3),
    "anolis": "python{}-pyyaml".format(2 if major_version==2 else 3),
    "bclinux": "python{}-pyyaml".format(2 if major_version==2 else 3),
    "centos": "python{}-pyyaml".format(2 if major_version==2 else 3),
    "culinux": "python{}-pyyaml".format(2 if major_version==2 else 3),
    "Deepin": "python{}-pyyaml".format("" if major_version==2 else 3),
    "rhel": "python{}-pyyaml".format("" if major_version==2 else 3)
}


def has_yum():
    if os.system("dnf --version &>/dev/null") == 0:
        os.system("dnf install -y yum &>/dev/null")
    if os.system("yum --version &>/dev/null") == 0:
        return True
    else:
        return False


def has_apt():
    if os.system("apt --version &>/dev/null") == 0:
        return True
    else:
        return False


def get_os_version():
    distro = ""
    version_id = ""
    with open("/etc/os-release") as os_release:
        info = os_release.readlines()
        for l in info:
            if l.startswith("ID="):
                distro = "".join(l.split("\"")).strip().split("=")[1]
                break
        for l in info:
            if l.startswith("VERSION_ID="):
                version_id = "".join(l.split("\"")).strip().split("=")[1]
                break
    if distro not in base_pkg_mapping:
        print("Unsupported OS distro: %s" %distro)
        if has_yum():
            print("Assume it is compatible to Anolis OS")
            distro = "anolis"
        elif has_apt():
            print("Assume it is compatible to Debian OS")
            distro = "debian"
        else:
            print("Unkown OS distro for tone")
    return (distro, version_id)


def install_package(package_mapping):
    distro, version = get_os_version()
    cmd = ""
    if has_apt():
        cmd += "apt update"
        if package_mapping[distro]:
            cmd += " && apt install -y {}".format(package_mapping[distro])
    elif has_yum():
        if package_mapping[distro]:
            cmd += "yum install -y {}".format(package_mapping[distro])
    else:
        print("== Error: Unsupported distro %s. ==" % distro)
        return False

    if os.system(cmd) == 0:
        return True
    else:
        return False


def install_base_pkg():
    if install_package(base_pkg_mapping):
        print("== Install Base Packages Done ==")
        return True
    else:
        print("== Install base packages failed. ==")
        return False


def install_pip_via_getpip():
    if major_version==2:
        cmd = "curl https://bootstrap.pypa.io/pip/2.7/get-pip.py -o /tmp/get-pip.py"
    else:
        cmd = "curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py"
    print("== CMD: {}".format(cmd))
    if os.system(cmd) != 0:
        print("== Get `get-pip.py` failed ==")
        return False

    if os.system("python /tmp/get-pip.py") != 0:
        print("== Install pip via get-pip.py failed ==")
        return False

    print("== Install pip via get-pip.py success. ==")
    return True


def install_pip():
    if install_package(pip_mapping):
        print("== Install pip success. ==")
        return True
    else:
        if not install_pip_via_getpip():
            print("== Install pip failed ==")
            return False
        else:
            print("== Install pip success. ==")
            return True


def install_pydev():
    if install_package(pydev_mapping):
        print("== Install python-devel success. ==")
        return True
    else:
        print("== Install python-devel failed ==")
        return False


def install_pyyaml():
    if install_package(pyyaml_mapping):
        print("== Install pyyaml via repo success. ==")
        return True

    print("== Install pyyaml failed from repo ==")
    print("== Try to install via pip ==")
    cmd = "pip{} install pyyaml".format(major_version)
    if os.system(cmd):
        print("== Install pyyaml via pip failed ==")
        return False
    print("== Install pyyaml via pip susccess. ==")
    return True


def install_psutil():
    if install_package(psutil_mapping):
        print("== Install psutil via repo success. ==")
        return True

    print("== Install psutil failed from repo ==")
    print("== Try to install via pip ==")
    cmd = "pip{} install psutil".format(major_version)
    if os.system(cmd):
        print("== Install psutil via pip failed ==")
        return False
    print("== Install psutil via pip susccess. ==")
    return True


if __name__ == "__main__":
    # install base packages
    if not install_base_pkg():
        sys.exit(1)
    if not install_pip():
        sys.exit(1)
    if not install_pydev():
        sys.exit(1)
    if not install_pyyaml():
        sys.exit(1)
    if not install_psutil():
        sys.exit(1)
    sys.exit(0)
