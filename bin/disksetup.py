#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pdb
import argparse
import glob
import subprocess
import os
import sys

DISK_PART_LABEL = 'tone'
DEBUG = False
SHOWCMD = False

base_root = os.path.dirname(
    os.path.dirname(
        os.path.realpath(__file__)))

"""
terms:
    disk: /sys/block/*
    part: /sys/block/${disk}/${part}
"""


def _debug(msg):
    if DEBUG:
        print("Debug: {}".format(msg))


def read_disk_major_map():
    is_block_session = False
    disk_majors = {}
    with open('/proc/devices') as f:
        for line in f:
            if is_block_session:
                (major, disk_type) = line.strip().split()
                disk_majors[major] = disk_type
            else:
                if line.startswith('Block devices'):
                    is_block_session = True
    return disk_majors


def read_disk_major():
    disk_major_map = read_disk_major_map()
    return disk_major_map.keys()


def reload_driver(driver):
    _debug('&' * 50)
    _debug(driver)
    fs = FileSystem()
    driver.unload()
    driver.unload()
    fs.umount_export()
    pre_disk = set(diskinfo._disks.keys())
    driver.load()
    post_disk = set(diskinfo._disks.keys())
    disk = {}
    for d in (post_disk - pre_disk):
        disk[d] = diskinfo._disks[d]
        if disk[d]['holder']:
            for hold_disk in disk[d]['holder'].split():
                disk[hold_disk] = diskinfo._disks[hold_disk]
    driver.set_disk(list(disk.keys()))
    return driver


def mkfs_mount(disk, filesystem, mkfsopt=None, mountopt=None):
    fs = FileSystem(fs=filesystem, mkfsopt=mkfsopt, mountopt=mountopt)
    lvs = [
        diskinfo._disks[d] for d in disk
        if diskinfo._disks[d]['type'] == 'lvm'
    ]
    if lvs:
        parts = lvs
    else:
        parts = [
            diskinfo._disks[d] for d in disk
            if diskinfo._disks[d]['type'] == 'part'
        ]
    for part in parts:
        fs.mkfs(part['device'])
        fs.mount(part['device'])
    diskinfo.collect_disk_info()


def umount_lvm():
    lvms = diskinfo.get_lvm_parts()
    fs = FileSystem()
    for part in lvms:
        if len(diskinfo._disks[part]['mountpoint']) == 0:
            continue
        fs.umount(diskinfo._disks[part]['mountpoint'])
    if lvms:
        fs.clean_vg()


class Exec(object):
    def __init__(self, command, dryrun=False):
        _debug(command)
        if SHOWCMD:
            print(command)
        if dryrun:
            return
        sp = subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True
        )
        sp.wait()
        self.stdout = sp.stdout.read()
        self.stderr = sp.stderr.read()
        self.returncode = sp.returncode


class Singleton(object):
    def __new__(cls, *args, **kw):
        if not hasattr(cls, '_instance'):
            cls._instance = object.__new__(cls)
        return cls._instance


class DiskInfo(object):
    """
    disks 的结构
    {
        key: {
            size: int,
            label: str,
            type: str, # disk, memory, lvm
            testdisk: bool,
            device: str, # /dev/vdb, /dev/ram0
            mountpoint: str,
            holder: str,
            }
        }
    }
    """
    # fields defined for lsblk command
    fields = [
        'NAME',
        'ROTA',
        'TYPE',
        'SIZE',
        'MOUNTPOINT',
        'LABEL',
    ]

    # recorders for disk information
    _disks = {}

    user_specified_disk = None

    def __init__(self):
        # super(object, self).__init__()
        # holder and device updated map
        self.disk_map_function = {
            'default': {
                'holder': lambda name: None,
            },
            'part': {
                'holder': self._get_part_holder,
            },
            'lvm': {
                'holder': self._get_lvm_holder_map,
            },
            'loop': {
                'holder': self._get_loop_holder_map,
            },
        }
        self.collect_disk_info()

    def _combine_device_path(self, name, disk_type=None):
        if disk_type == 'lvm':
            return os.path.join('/dev', 'mapper', name)
        else:
            return os.path.join('/dev', name)

    def collect_disk_info(self):
        global SHOWCMD
        bk = SHOWCMD
        majors = read_disk_major()
        cmd = "lsblk -b -P -o {}".format(','.join(self.fields))
        cmd += " -I {}".format(','.join(majors))
        SHOWCMD = False
        process = Exec(cmd)
        SHOWCMD = bk
        if process.returncode == 0:
            recorders = self._parse(process.stdout.decode())
            self._resolve_hierarchy(recorders)

    def _parse(self, output):
        """Parse lsblk command outputs

        Args:
            output ([str]): lsblk commands with option '-p' outputs

        Returns:
            recorders ([dict]): dict element's keys are defined in self.fields
        """
        recorders = {}
        for line in output.split("\n"):
            if line.strip() == '':
                continue
            raw_vars = {}
            exec (
                line.replace(' ', ';'),
                raw_vars
            )
            rec = {}
            for field in self.fields:
                rec[field.lower()] = raw_vars[field]
            recorders[rec['name']] = rec
        # print(recorders)
        return recorders

    def show_disk(self):
        for disk, rec in self._disks.items():
            if rec['holder'] is not None and rec['holder'] in self._disks:
                continue
            print(disk, rec)
            for part, el in [(k, v) for k, v in self._disks.items()
                             if v['holder'] == disk]:
                print("\t", part, el)
                for lvm, info in [(k, v) for k, v in self._disks.items()
                                  if v['holder'] == part]:
                    print("\t", lvm, info)

    def get_disk_size(self, disk):
        if disk in self._disks:
            return self._disks[disk]['size']

    def get_disk_device(self, disk):
        if disk in self._disks:
            return self._disks[disk]['device']

    def get_disk_with_parts(self, disk):
        disks = {}
        if disk in self._disks:
            disks = {}
            disks[disk] = self._disks[disk]
            for k, rec in self._disks.items():
                if rec['holder'] and disk in rec['holder'].split():
                    disks[k] = rec
        return disks

    def _get_part_holder(self, part):
        dirs = glob.glob("/sys/block/*/{}".format(part))
        if len(dirs) > 1:
            print("Warning: Find more part {}".format(part))
        host = dirs[0].split(os.path.sep)[-2]
        return host

    def _get_lvm_holder_map(self, diskname):
        names = glob.glob("/sys/block/*/dm/name")
        mapper = {}
        for name in names:
            lvm = None
            with open(name) as f:
                for line in f:
                    lvm = line.strip()
            tmp = name.split(os.path.sep)[:-2]
            tmp.append('slaves')
            slaves = os.path.sep.join(tmp)
            holders = glob.glob("{}/*".format(slaves))
            if lvm is not None:
                mapper[lvm] = ' '.join(
                    [os.path.basename(holder) for holder in holders]
                )
        return mapper[diskname]

    def _get_loop_holder_map(self, diskname):
        names = glob.glob("/sys/block/*/loop/backing_file")
        mapper = {}
        for name in names:
            holder = None
            with open(name) as f:
                for line in f:
                    holder = line.strip()
            loop = name.split(os.path.sep)[3]
            mapper[loop] = holder
        if diskname in mapper:
            return mapper[diskname]
        return self._get_part_holder(diskname)

    def _check_mountpoint(self, data):
        return data['mountpoint'].startswith('/fs/') or \
               data['mountpoint'].startswith('/export/')

    def _set_testdisk(self, data, value=True):
        data['testdisk'] = value

    def _is_test_part(self, part):
        data = self._disks[part]
        if self._check_mountpoint(data):
            self._set_testdisk(data)
            return True
        if data['mountpoint'] == '':
            holders = set([
                self._is_test_lvm(lvm)
                for lvm, info in self._disks.items()
                if info['holder'] and \
                   part in info['holder'].split()
            ])
            if False not in holders:
                self._set_testdisk(data)
                return True
        return False

    def _is_test_lvm(self, lvm):
        data = self._disks[lvm]
        if data['mountpoint'] == '':
            self._set_testdisk(data)
            return True
        if self._check_mountpoint(data):
            self._set_testdisk(data)
            return True
        return False

    def _is_test_disk(self, disk):
        data = self._disks[disk]
        if self._check_mountpoint(data):
            self._set_testdisk(data)
            return True
        holders = set([
            info['testdisk']
            for part, info in self._disks.items()
            if info['holder'] and \
               disk in info['holder'].split()
        ])
        if len(holders):
            if False in holders:
                for _, info in self._disks.items():
                    if info['holder'] and \
                            disk in info['holder'].split():
                        self._set_testdisk(info, False)
                return False
            else:
                self._set_testdisk(data)
                return True
        else:
            if data['mountpoint'] == '':
                self._set_testdisk(data)
                return True
            else:
                return False

    def _set_disk_usage_info(self):
        for name, info in self._disks.items():
            if info['type'] == 'part':
                self._is_test_part(name)
        for name, info in self._disks.items():
            if info['type'] == 'disk':
                self._is_test_disk(name)

    def _resolve_hierarchy(self, data):
        """Resolve the disk hierarchy structure
        Save the recorders in self._disk and each recorder has holder
        key to recording the root disk information.

        Args:
            data ([disk]): return from _parse
        """
        disks = {}
        for name, info in data.items():
            if info['type'] not in ('disk', 'part', 'lvm', 'loop'):
                continue
                #print("Warning: disk type {} not in disk, part, lvm, loop".format(info['type']))
                #print(name)
            disk = {
                'size': int(info['size']),
                'label': info['label'],
                'type': info['type'],
                'testdisk': False,
                'device': self._combine_device_path(name, info['type']),
                'mountpoint': info['mountpoint'],
                'holder': None,
            }
            if disk['size'] == 0:
                continue
            if info['type'] in self.disk_map_function:
                disktype = info['type']
            else:
                disktype = 'default'
            disk['holder'] = self.disk_map_function[disktype]['holder'](name)

            disks[name] = disk
        self._disks = disks
        self._set_disk_usage_info()
        if DEBUG:
            self.show_disk()

    def get_test_disks(self):
        disks = [disk for disk, rec in self._disks.items()
                 if rec['holder'] is None and rec['testdisk']]
        if self.user_specified_disk:
            if self.user_specified_disk not in disks:
                raise Exception(
                    "{} can not be used for test".format(
                        self.user_specified_disk))
            else:
                if disks.index(self.user_specified_disk) != 0:
                    tmp = [self.user_specified_disk]
                    tmp.extend([d for d in disks if d != self.user_specified_disk])
                    disks = tmp
        return disks

    def get_test_parts(self):
        parts = [disk for disk, rec in self._disks.items()
                 if rec['holder'] is not None and rec['testdisk']]
        return parts

    def get_lvm_parts(self):
        parts = self.get_test_parts()
        lvms = [
            p for p in parts if p.startswith('vgdata-lvm')
        ]
        return lvms

    def get_test_mountpoints(self):
        mountpoints = [rec['mountpoint'] for _, rec in self._disks.items()
                       if rec['mountpoint'] != '' and rec['testdisk']]
        return mountpoints

    def get_devices_with_pattern(self, pattern, keywords=None):
        devices = glob.glob(pattern)
        prepath = '/sys/block/'
        if keywords:
            finded = []
            for fn in devices:
                with open(fn) as fh:
                    content = fh.read()
                if keywords in content:
                    dev = fn.repace(prepath, '')
                    finded.append(dev[:dev.index('/')])
            devices = finded
        devs = {}
        for device in devices:
            dev = os.path.basename(device)
            if dev in self._disks:
                devs[dev] = self._disks[dev]
        return devs


class Disk(object):
    dev = ""
    disk = {}

    def __init__(self, dev=None, nparts=1):
        global SHOWCMD
        SHOWCMD = True
        self.nparts = nparts
        self.dev = dev
        # super(object, self).__init__()

    def __eq__(self, o):
        if hasattr(object, 'dev'):
            return self.dev == object.dev
        return False

    def set_disk(self, disk):
        self.disk = disk

    def load(self):
        raise NotImplementedError('function load')

    def unload(self):
        raise NotImplementedError('function unload')

    def parted(self):
        fs = FileSystem()
        fs.parted(self.dev, self.nparts)

    def wipefs(self):
        fs = FileSystem()
        fs.wipefs(self.dev)

    def umountall(self):
        disks = diskinfo.get_disk_with_parts(self.dev)
        worked = False
        fs = FileSystem()
        for _, disk in disks.items():
            if disk['mountpoint'] == '':
                continue
            fs.umount(disk['mountpoint'])
            worked = True
        return worked

    def _exec(self, cmd, dryrun=False):
        runner = Exec(cmd, dryrun=dryrun)
        if dryrun:
            return ""
        if runner.returncode != 0:
            raise Exception("Fail: {}\n\n{}".format(
                cmd,
                runner.stderr.decode()
            ))
        return runner.stdout.decode()

    def _exec_code(self, cmd, dryrun=False):
        runner = Exec(cmd, dryrun=dryrun)
        if dryrun:
            return 0
        return runner.returncode


class ModuleDriveDisk(Disk):
    """module驱动的磁盘设置, 如: brd, null
    """
    params_map = {
        'brd': {
            'module': 'brd',
            'params': ['rd_nr={}', 'rd_size={}'],
            'pattern': '/dev/ram*',
            'canmount': False,
        },
        'null': {
            'module': 'null_blk',
            'params': ["nr_devices={}", ''],
            'pattern': '/dev/nullb*',
            'canmount': False,
        },
        'scsi': {
            'module': 'scsi_debug',
            'params': ["add_host=1 num_tgts={}", "max_luns=1 dev_size_mb={}"],
            'pattern': '/sys/block/*/device/model',
            'keyword': 'scsi_debug',
            'canmount': False,
        },
        # 'peme': {
        #     'module': 'nd_e820',
        #     'params': ["", ""],
        #     'name4major': 'ramdisk',
        # },
    }

    def __init__(self, dev, nparts=0, size=0):
        super(ModuleDriveDisk, self).__init__(dev, nparts)
        if dev not in self.params_map:
            raise Exception("Not support module: <{}>".format(dev))
        self.params = [nparts, size]
        self.kernel_module = self.params_map[dev]['module']
        self.load_cmd = self.gen_cmd()
        self.enabled = self._check_module()

    def unload(self):
        if not self.enabled:
            raise Exception("Not support module: <{}>".format(self.dev))
        cmd = "modprobe -r {}".format(self.params_map[self.dev]['module'])
        self._exec(cmd)
        diskinfo.collect_disk_info()

    def load(self):
        if not self.enabled:
            raise Exception("Not support module: <{}>".format(self.dev))
        diskinfo.collect_disk_info()
        pre_disk = set(diskinfo._disks.keys())
        self._exec(self.load_cmd)
        diskinfo.collect_disk_info()
        post_disk = set(diskinfo._disks.keys())
        new_disk = post_disk - pre_disk
        if self.params_map[self.dev]['canmount']:
            fs = FileSystem()
            for d in new_disk:
                fs.parted(d, 1)
            diskinfo.collect_disk_info()

    def gen_cmd(self):
        param = self.params_map[self.dev]
        cmd = ['modprobe', param['module']]
        for i, p in enumerate(param['params']):
            if p == "":
                continue
            if self.params[i] == 0:
                continue
            cmd.append(p.format(self.params[i]))
        return " ".join(cmd)

    def _check_module(self):
        cmd = "modinfo {}".format(self.kernel_module)
        ret = self._exec_code(cmd, dryrun=False)
        return ret == 0

    @classmethod
    def clean_disk(cls):
        """卸载测试中添加的内存磁盘模块
        """
        cmd = "lsmod"
        runner = Exec(cmd)
        if runner.returncode != 0:
            raise Exception("Run command <{}>".format(cmd))
        output = runner.stdout.decode().strip().split("\n")
        output.pop(0)
        loaded_modules = {}
        unloaded_modules = {}
        fs = FileSystem()
        for line in output:
            if line == '':
                continue
            loaded_modules[line.split()[0]] = True
        for m, p in cls.params_map.items():
            if p['module'] in loaded_modules:
                print(m, p['module'])
                disks = diskinfo.get_devices_with_pattern(
                    p['pattern'],
                    keywords=p['keyword'] if 'keyword' in p else None
                )
                print(disks)
                for disk, info in disks.items():
                    if info['testdisk']:
                        if info['mountpoint'] != '':
                            fs.umount(info['mountpoint'])
                    else:
                        if m not in unloaded_modules:
                            unloaded_modules[m] = {}
                        unloaded_modules[m][disk] = info
                if len(unloaded_modules.keys()) == 0:
                    runner = Exec("modprobe -r {}".format(p['module']))
                    if runner.returncode != 0:
                        raise Exception("Can not unload <{}>".format(
                            p['module']
                        ))
        return unloaded_modules


class NFSDisk(Disk):
    """服务驱动的磁盘设置, 如: nfs
    """
    export_dir = '/export'
    conf = '/etc/exports'

    def __init__(self, dev, filesystem, nparts=1):
        super(NFSDisk, self).__init__()
        self.dev = dev
        self.filesystem = filesystem
        self.exports = []
        self.nparts = nparts
        self.physical_disk = None

    def load(self):
        self.physical_disk = DiskDisk('disk', self.nparts)
        reload_driver(self.physical_disk)
        mkfs_mount(self.physical_disk.disk, self.filesystem)
        self.server_setup()
        diskinfo.collect_disk_info()

    def unload(self):
        umount_lvm()
        if not self.physical_disk:
            self.physical_disk = DiskDisk('disk', self.nparts)
        NFSDisk.clean_up()
        self.physical_disk.unload()
        diskinfo.collect_disk_info()

    def server_setup(self):
        self._install_nfs()
        self._setup_export()
        self._setup_export_disk()
        self._start_nfs_server()

    def _install_nfs(self):
        """
        Check and install nfs util tools
        """
        script = (
            "#!/bin/bash\n"
            ". {}\n"
            "export TONE_BM_RESULT_DIR=/tmp/$$\n"
            "mkdir $TONE_BM_RESULT_DIR\n"
            "install_pkg nfs-utils nfs4-acl-tools\n"
            "rm -rf $TONE_BM_RESULT_DIR\n"
        )
        script = script.format(
            os.path.join(base_root, 'lib', 'common.sh'))
        fn = "/tmp/nfs_dep_{}".format(os.getpid())
        with open(fn, "w") as fh:
            fh.write(script)
        os.chmod(fn, 0o755)
        task = Exec(fn)
        os.remove(fn)
        if task.returncode != 0:
            # TODO: user tone_print to replace below message
            # print('Run Error', file=sys.stderr)
            sys.exit(1)

    def _setup_export(self):
        """
        Setup /export directory and mount tempfs to /export
        """
        fs = FileSystem()
        fs.create_dir(self.export_dir)
        if fs.is_mountpoint(self.export_dir):
            print("/export is mountpoint")
        else:
            Exec(
                "mount -t tmpfs 'nfsv4_root_export' {}".format(
                    self.export_dir
                ))
        self._update_export_conf(
            "{} *(fsid=0,rw,no_subtree_check,no_root_squash)".format(
                self.export_dir
            ))

    def _update_export_conf(self, setting):
        """
        Write disk to /etc/exports file
        """
        with open(self.conf, 'a+') as fh:
            found = False
            for line in fh:
                if line.find(setting) > -1:
                    found = True
                    break
            if not found:
                fh.seek(0, 2)
                fh.write("{}\n".format(setting))

    def _setup_export_disk(self):
        mountpoints = []
        for dev in self.physical_disk.disk:
            devinfo = diskinfo._disks[dev]
            if devinfo['mountpoint'] and devinfo['mountpoint'] != "":
                mountpoints.append(devinfo['mountpoint'])
        for mp in mountpoints:
            if mp[0] == '/':
                export_path = mp[1:]
            export_path = os.path.join(self.export_dir, export_path)
            fs = FileSystem(mountopt="--bind")
            fs.create_dir(export_path)
            fs.mount(mp, export_path)
            self._update_export_conf(
                "{} *(rw,no_subtree_check,no_root_squash)".format(
                    export_path))
            self.exports.append(export_path)
            # print(export_path)
        self.set_disk(self.exports)
        # pdb.set_trace()
        diskinfo.collect_disk_info()

    def _start_nfs_server(self):
        """:w

        Start nfs service: rpcbind and nfs-server
        """
        Exec("systemctl restart rpcbind")
        Exec("systemctl restart nfs-server")

    @classmethod
    def _unexport(cls, path):
        fs = FileSystem()
        try:
            fs.unexport(path)
        except Exception as e:
            pass
        cls._clean_export(path)
        fs.umount(path)

    @classmethod
    def _clean_export(cls, path):
        content = []
        with open(cls.conf) as fh:
            content = [i for i in fh if not i.startswith(path)]
        with open(cls.conf, 'w') as fh:
            if content:
                fh.write("".join(content))
            else:
                fh.write("")

    @classmethod
    def _get_nfs_mountpoints(cls):
        mps = None
        with open('/proc/self/mounts') as fh:
            mps = [
                line.split()[1]
                for line in fh if line.split()[1].startswith('/export/')
            ]
        return mps

    @classmethod
    def clean_up(cls):
        isclean = False
        for mp in NFSDisk._get_nfs_mountpoints():
            cls._unexport(mp)
            isclean = True
        if isclean:
            cls._unexport(cls.export_dir)


class DeviceMapperDisk(Disk):
    vgname = 'vgdata'

    def __init__(self, dev, nparts=1):
        if not self.has_lvm():
            print("Error: Please install lvm first")
            sys.exit(1)
        super(DeviceMapperDisk, self).__init__(dev=dev, nparts=nparts)
        self.physical_disk = None
        self.used_disk = []

    def load(self):
        self.physical_disk = DiskDisk('disk', self.nparts)
        reload_driver(self.physical_disk)
        self.create_lv()
        diskinfo.collect_disk_info()

    def unload(self):
        umount_lvm()
        if not self.physical_disk:
            self.physical_disk = DiskDisk('disk', self.nparts)
        self.physical_disk.unload()
        diskinfo.collect_disk_info()

    def _create_pv(self):
        fs = FileSystem()
        used_parts = []
        for disk in self.physical_disk.disk:
            dinfo = diskinfo._disks[disk]
            if dinfo['type'] == 'part':
                fs.wipefs(disk)
                used_parts.append(dinfo['device'])
                self.used_disk.append(dinfo['device'])
        cmd = "pvcreate -f {}".format(' '.join(used_parts))
        Exec(cmd)

    def _create_vg(self):
        cmd = "vgcreate vgdata {}".format(
            ' '.join(self.used_disk)
        )
        Exec(cmd)

    def create_lv(self):
        umount_lvm()
        self._create_pv()
        self._create_vg()
        ratio = int(100 / self.nparts)
        for nr in range(self.nparts):
            cmd = "lvcreate -y -l {}%VG -n lvm{} {}".format(
                ratio,
                nr + 1,
                self.vgname
            )
            Exec(cmd)
        diskinfo.collect_disk_info()
        disk = glob.glob("/dev/mapper/{}-*".format(self.vgname))
        return disk

    def has_lvm(self):
        cmd = "command -v lvm"
        task = Exec(cmd)
        if task.returncode == 0:
            return True
        else:
            return False


class DiskDisk(Disk):
    """使用已有磁盘，即已挂载的磁盘
    """

    def __init__(self, dev, nparts=1):
        super(DiskDisk, self).__init__(dev=dev, nparts=nparts)
        self._get_test_disk()
        self.nparts = nparts

    def _get_test_disk(self):
        test_disks = diskinfo.get_test_disks()
        if test_disks:
            self.dev = test_disks[0]
        else:
            raise Exception('No test disks found')

    def load(self):
        self.parted()
        diskinfo.collect_disk_info()

    def unload(self):
        self.umountall()
        self.wipefs()
        diskinfo.collect_disk_info()


class DiskManager(Singleton):
    """
    """
    _disks = {}

    def __init__(self, disk_type, nparts, size=0):
        self._disks[disk_type] = self.create_disk_driver(
            disk_type, nparts, size
        )
        driver = self._disks[disk_type]
        reload_driver(driver)

    def get_parts(self, disk):
        parts = []
        if disk and disk in self._disks:
            recorders = diskinfo.get_disk_with_parts(
                self._disks[disk].dev
            )
            for _, rec in recorders.items():
                if rec['holder'] is None:
                    continue
                parts.append(rec)
        return parts

    def create_disk_driver(self,
                           disk_type,
                           nparts,
                           size,
                           filesystem="ext4"):
        """
        """
        disks_driver_map = {
            'brd': ModuleDriveDisk,
            'null': ModuleDriveDisk,
            'scsi': ModuleDriveDisk,
            'scsi_debug': ModuleDriveDisk,
            'peme': ModuleDriveDisk,
            'nfs': NFSDisk,
            'dm': DeviceMapperDisk,
            'disk': DiskDisk,
        }

        bname = os.path.basename(disk_type)
        if disk_type not in disks_driver_map and \
                not disk_type.startswith('nfs'):
            if bname in diskinfo.get_test_disks():
                disk_type = 'disk'
                diskinfo.user_specified_disk = bname
            else:
                raise Exception("Can not process disk {}".format(disk_type))

        if disk_type in ('dm', 'disk'):
            driver = disks_driver_map[disk_type](dev=disk_type, nparts=nparts)
        elif disk_type.startswith('nfs'):
            driver = disks_driver_map['nfs'](
                dev=disk_type,
                filesystem=filesystem,
                nparts=nparts)
        else:
            driver = disks_driver_map[disk_type](dev=disk_type, nparts=nparts, size=size)
        return driver

    @classmethod
    def clean_disk(cls):
        ModuleDriveDisk.clean_disk()


class FileSystem(object):
    mount_opt_map = {
        'xfs': '-o inode64',
        'virtio_fs': '-o rootmode=040000,user_id=0,group_id=0,dax,\
default_permissions,allow_other',
    }

    def __init__(self, fs='ext3', mkfsopt="", mountopt=""):
        # super(object, self).__init__()
        self.vgname = 'vgdata'
        if fs is None:
            fs = 'ext3'
        if mkfsopt is None:
            mkfsopt = ''
        if mountopt is None:
            mountopt = ''

        self._fileformat = fs
        if fs.startswith('ext'):
            self._mkfs_options = "-t {} -L tone -q -F {}".format(
                fs,
                mkfsopt
            ).strip()
        else:
            self._mkfs_options = "-t {} -L tone -q -f {}".format(
                fs,
                mkfsopt
            ).strip()

        if mountopt == "":
            if fs in self.mount_opt_map:
                self._mount_options = self.mount_opt_map[fs]
            elif fs.startswith('nfs'):
                self._mount_options = "-o vers={}".format(fs[3:])
            else:
                self._mount_options = ""
        else:
            self._mount_options = mountopt

    def _validate_fs_format(self, format):
        cmd = "find /usr/bin /usr/sbin /usr/local/bin /sbin -name 'mkfs*'"
        output = self._exec(cmd).split()
        validated = False
        for i in output:
            ext = os.path.splitext(i)
            if len(ext) < 2:
                continue
            if format == ext[-1][1:]:
                validated = True
                break
        return validated

    def mkfs(self, part):
        if not self._validate_fs_format(self._fileformat):
            raise Exception("Error: not support: {}".format(self._fileformat))

        cmd = "mkfs {} {}".format(self._mkfs_options, part)
        output = self._exec(cmd, dryrun=False)

    def mount(self, part, dest=None):
        if dest:
            mountpoint = dest
        else:
            mountpoint = '/fs/{}'.format(os.path.basename(part))
            self.create_dir(mountpoint)
        cmd = "mount {} {} {}".format(self._mount_options, part, mountpoint)
        output = self._exec(cmd, dryrun=False)

    def create_dir(self, dir):
        _debug("Create Dir: {}".format(dir))
        if not os.path.exists(dir):
            os.makedirs(dir)

    def unexport(self, dir):
        cmd = "exportfs -u *:{}".format(dir)
        self._exec(cmd)

    def _exec(self, cmd, dryrun=False):
        runner = Exec(cmd, dryrun=dryrun)
        if dryrun:
            return
        if runner.returncode != 0:
            raise Exception("Fail: {}\n\n{}".format(
                cmd,
                runner.stderr.decode()
            ))
        return runner.stdout.decode()

    def umount(self, mountpoint):
        cmd = "umount -l -f {}".format(mountpoint)
        self._exec(cmd)
        diskinfo.collect_disk_info()

    def is_mountpoint(self, path):
        task = Exec("mountpoint {}".format(path))
        return task.returncode == 0

    def umount_export(self):
        if self.is_mountpoint('/export'):
            self.umount('/export')

    def wipefs(self, dev):
        cmd = "wipefs -a -f {}".format(
            diskinfo.get_disk_device(dev)
        )
        self._exec(cmd)

    def _check_vg(self):
        cmd = "vgs"
        task = Exec(cmd)
        for line in task.stdout.decode('utf-8').split("\n"):
            if line.strip() == "":
                continue
            if self.vgname == line.strip().split()[0]:
                return True
        return False

    def clean_vg(self):
        if self._check_vg():
            cmd = "vgremove -f {}".format(self.vgname)
            Exec(cmd)

    def parted(self, dev, nparts):
        total_size = diskinfo.get_disk_size(dev)
        each_disk_size = int(total_size / nparts) >> 20
        total_size = total_size >> 20
        _debug("Debug: {} mb".format(each_disk_size))
        end = each_disk_size
        device = diskinfo.get_disk_device(dev)
        self._part_runner(
            device,
            ismain=True,
            start=0,
            end=end
        )
        for i in range(1, nparts):
            start = end
            if i == nparts - 1:
                end = total_size
            else:
                end += each_disk_size
            self._part_runner(
                device,
                ismain=False,
                start=start,
                end=end
            )

    def _part_runner(self, disk, ismain=True, start=None, end=None):
        if ismain:
            cmd = "parted -s {} mklabel gpt mkpart primary {} {}"
        else:
            cmd = "parted -s {} mkpart primary {} {}"
        cmd = cmd.format(disk, start, end)
        self._exec(cmd)


diskinfo = DiskInfo()

"""
./disk.py --device disk --partitionnumber 2
"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--device', default='disk', help="disk, brd, null etc")
    parser.add_argument('--partitionnumber', default=1, type=int, help="Partition number")
    parser.add_argument('--memdisksize', default=0, type=int, help="Disk size for memory disk")
    parser.add_argument('--size', default=0, type=int, help="Disk size")
    parser.add_argument('--filesystem', type=str, help="Partition number")
    parser.add_argument('--mkfsopt', type=str, help="mkfs command option")
    parser.add_argument('--mountopt', type=str, help="mount command option")
    parser.add_argument('--umount', action='store_true', help="mount command option")

    config = parser.parse_args()
    is_nfs = False

    if config.umount:
        umount_lvm()
        NFSDisk.clean_up()
        disk = DiskDisk('disk')
        disk.umountall()
        disk.umountall()
    elif config.device:
        umount_lvm()
        ModuleDriveDisk.clean_disk()
        diskinfo.collect_disk_info()
        device_name = config.device
        diskmanager = DiskManager(device_name, config.partitionnumber, size=config.memdisksize)
        if (not config.device.startswith('nfs')) and config.filesystem:
            print(DiskManager._disks[device_name].disk)
            mkfs_mount(
                DiskManager._disks[device_name].disk,
                config.filesystem,
                mkfsopt=config.mkfsopt,
                mountopt=config.mountopt)
